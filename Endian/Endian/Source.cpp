#include <iostream>

void ex0()
{
   unsigned int i = 1;
   char *c = (char*)&i;
   if (*c)
      printf("Little endian");
   else
      printf("Big endian");
   getchar();

}

void ex1()
{
   float x = 0.5;
   double y = 0.1;
   if (x == 0.1) {
      std::cout << "if";
   } else if (x == 0.1f) {
      std::cout << "else if";
   } else {
      std::cout << "else";
   }

}

bool comp(float A, float B,float epsilon)
{
   float diff = A - B;
   return (diff < epsilon) && (-diff < epsilon);
}

void ex2()
{
   float x;
   float y;
   float expected;

   std::cout << "x:";
   std::cin >> x;
   std::cout << "y:";
   std::cin >> y;
   std::cout << "expected";
   std ::cin >> expected;

   float actual = x*y;

   float epsilon = 0.0001;

   std::cout << comp(actual, expected, epsilon);
}

#define six 6;

int main()
{
   
   ex2();

   int one = 1;
   int seven = six + one;
   

   return 0;
}